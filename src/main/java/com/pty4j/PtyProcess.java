package com.pty4j;

import android.os.Build;
import android.util.Log;

import com.pty4j.unix.Pty;
import com.pty4j.unix.UnixPtyProcess;
import com.sun.jna.Platform;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Process with pseudo-terminal(PTY).
 * On Unix systems the process is created with real pseudo-terminal (PTY).
 * <p>
 * On Windows, ConPTY is used. If unavailable, WinPty is used.
 * <p>
 * Note that on Unix to be sure that no file descriptors are left unclosed after process termination
 * one of two things should be accomplished:
 * 1) Streams returned by getInputStream() and getOutputStream() method should be acquired and closed
 * 2) Method destroy() should be invoked even after the process termination
 * <p>
 *
 * @author traff
 */
public abstract class PtyProcess extends Process {
  public abstract void setWinSize(WinSize winSize);

  public abstract @NotNull WinSize getWinSize() throws IOException;

  /**
   * @return byte to send to process's input on Enter key pressed
   */
  public byte getEnterKeyCode() {
    return '\r';
  }

  @SuppressWarnings("unused") // used in IntelliJ
  public boolean isConsoleMode() {
    return false;
  }

  /**
   * @deprecated use {@link #isAlive()} instead
   */
  @Deprecated
  public boolean isRunning() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      return isAlive();
    } else {
      Log.w(getClass().getSimpleName(), "isRunning return true");
      return true;
    }
  }

  /**
   * @deprecated use {@link #()} instead
   */
  public int getPid() {
    return (int)android.os.Process.myPid();
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command) throws IOException {
    return exec(command, (Map<String, String>)null);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, Map<String, String> environment) throws IOException {
    return exec(command, environment, null, false, false, null);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, Map<String, String> environment, String workingDirectory) throws IOException {
    return exec(command, environment, workingDirectory, false, false, null);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, String[] environment) throws IOException {
    return exec(command, environment, null, false);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, String[] environment, String workingDirectory, boolean console) throws IOException {
    return new UnixPtyProcess(command, environment, workingDirectory, new Pty(console), console ? new Pty() : null, console);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, Map<String, String> environment, String workingDirectory, boolean console)
    throws IOException {
    return exec(command, environment, workingDirectory, console, false, null);
  }

  /** @deprecated use {@link PtyProcessBuilder} instead */
  @Deprecated
  public static PtyProcess exec(String[] command, Map<String, String> environment, String workingDirectory, boolean console, boolean cygwin,
                                File logFile) throws IOException {
    PtyProcessBuilder builder = new PtyProcessBuilder(command)
        .setEnvironment(environment)
        .setDirectory(workingDirectory)
        .setConsole(console)
        .setCygwin(cygwin)
        .setLogFile(logFile);
    return builder.start();
  }
}
